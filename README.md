# telepub

File publisher to telegram

## Getting Started
Для работы бота нужно указать:
TELEPUB_BOT_CHANNEL_ID=
TELEPUB_BOT_TOKEN=

И теперь запускаем отправку сообщения:
dart main.dart "Моё сообщение от бота"

Если нужно отправить файл, то:
dart main.dart "Подпись к файлу" "Путь к файлу"
