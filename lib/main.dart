import 'dart:io';

import 'package:teledart/teledart.dart';
import 'package:teledart/telegram.dart';

Future<void> main(List<String> args) async {
  final botToken = Platform.environment['TELEPUB_BOT_TOKEN'];
  final chatId = Platform.environment['TELEPUB_BOT_CHANNEL_ID'];
  final userName = (await Telegram(botToken!).getMe()).username!;
  final String message = args[0];

  var teleDart = TeleDart(botToken, Event(userName));
  teleDart.start();

  if (args.length == 1) {
    teleDart.sendMessage(chatId, message);
    exit(0);
  }
  if (args.length == 2) {
    String filePath = args[1];
    var file = File(filePath);
    var result = await teleDart.sendDocument(chatId, file, caption: message);
    exit(0);
  }
  print('args.length must be 0 or 1.');
  exit(1);
}
